import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {UsersModule} from './users/users.module';
import {SessionsModule} from './call-sessions/sessions.module';
import {ConsultantProfilesModule} from './consultant-profiles/consultant-profiles.module';
import {AuthModule} from './auth/auth.module';
import {ProblemsModule} from './problems/problems.module';
import {AwsModule} from './aws/aws.module';
import {ConfigModule, ConfigService} from '@nestjs/config';
import {MailerModule} from '@nestjs-modules/mailer';
import {NotificationsModule} from './notifications/notifications.module';
import {ElasticSearchModule} from './elastic-search/elastic-search.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { ReviewsModule } from './reviews/reviews.module';
import {Review} from './reviews/reviews.model';

@Module({
    imports: [UsersModule, SessionsModule, ConsultantProfilesModule, AuthModule, ProblemsModule, AwsModule, MailerModule,ReviewsModule,
        ConfigModule.forRoot({
            envFilePath: ['.env.local', '.env.test', '.env.staging', '.env.production'],
            isGlobal: true,
        }),
        ConfigService,
        MailerModule.forRootAsync(
            {
                imports: [ConfigModule],
                useFactory: async (configService: ConfigService) => ({
                    transport: {
                        host: configService.get('TRANSPORT_CHANNEL'),
                        port: 587,
                        secure: false, // upgrade later with STARTTLS
                        auth: {
                            user: configService.get('TRANSPORT_AUTH_EMAIL'),
                            pass: configService.get('TRANSPORT_AUTH_EMAIL_PASSWORD'),
                        },
                    },
                    defaults: {
                        from: configService.get('EMAIL_SENDER'),
                    },
                    template: {
                        dir: __dirname + '/templates',
                        options: {
                            strict: true,
                        },
                    },
                }),
                inject: [ConfigService]
            }
        ),
        ConfigModule,
        NotificationsModule,
        ElasticSearchModule,
        DashboardModule,
        ReviewsModule],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}
