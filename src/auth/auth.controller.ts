import { Controller, Get, Post, Body, UseGuards, UseInterceptors } from '@nestjs/common';
import { AuthService } from './auth.service';
import { RegisterDTO } from './dto/register.dto';
import { LoginDTO } from './dto/login.dto';
import {CurrentUser} from '../common/custom-decorators/current.user'
import { AuthGuard } from "@nestjs/passport";
import { ForbiddenInterceptor } from '../common/interceptors/forbidden.interceptor';
import { InternelServerErrorInterceptor } from '../common/interceptors/internel.server.error.interceptor';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService:AuthService) {}

  @Post('login')
  @UseInterceptors(new ForbiddenInterceptor('User with this credentials does not exist'))
  Login(@Body() loginBody:LoginDTO) {
    return this.authService.loginUser(loginBody.email, loginBody.password ,loginBody.deviceUniqueToken, loginBody.deviceType)
  }

  @Post('register')
  @UseInterceptors(new InternelServerErrorInterceptor())
  Register(@Body() registrationBody:RegisterDTO) {
    return this.authService.registerUser(registrationBody)
  }

  @Post('facebook/login')
  socialLoginOrRegisterFacebook(@Body() facebookLoginToken) {
      return this.authService.socialLogin(facebookLoginToken,null)
  }

  @Post('google/login')
  socialLoginOrRegisterGoogle(@Body() googleLoginToken) {
    return this.authService.socialLogin(null,googleLoginToken)
  }

  @Get('test')
  @UseGuards(AuthGuard('jwt'))
  Test(@CurrentUser() request: any) {
    console.log(request);
    return 'hello world'
  }

}
