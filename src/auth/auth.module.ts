import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { DatabaseModule } from '../database/database.module';
import { UsersModule } from '../users/users.module';
import { JwtStrategy } from './jwt.strategy';
import { ConfigModule } from '@nestjs/config';
import { MailerModule } from '@nestjs-modules/mailer';

@Module({
  imports:[UsersModule,DatabaseModule,ConfigModule,MailerModule],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy],
  exports:[AuthModule]
})
export class AuthModule {}
