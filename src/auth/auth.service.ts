import {
    ForbiddenException,
    HttpException,
    HttpStatus,
    Inject,
    Injectable,
    InternalServerErrorException,
    NotFoundException,
} from '@nestjs/common';
import { Model } from 'mongoose';
import * as bcrypt from 'bcrypt';
import errorCode from '../common/mongo.error.enum';
import errorMessages from '../common/error.enums';
import successMessages from '../common/success.message.enums';
import DuplicationKeyException from '../common/exceptions/duplication.key.exception';
import {Response} from '../common/response';
import { IUser } from '../users/interface/user.interface';
import {JwtPayload} from "./interfaces/jwt-payload.interface";
import { ConfigService } from '@nestjs/config';
const  jwtToken = require ('jsonwebtoken');

@Injectable()
export class AuthService {
    Response = new Response();
    constructor(@Inject('USER_MODEL') private readonly userModel:Model<IUser> , private readonly configService:ConfigService
    ){}

    async registerUser(registrationBody) :Promise<Record<string, any>> {
        try {
            const hashedPassword = await bcrypt.hash(registrationBody.password, 10);
            const registeredUser = await this.userModel.create({...registrationBody,password:hashedPassword});
            if(registeredUser) {
                console.log(registeredUser)
                return  this.Response.returnJSONResponse(successMessages.USER_SIGNUP_SUCCESS,HttpStatus.CREATED,null)
            } else {
                return  new InternalServerErrorException();
            }
        } catch (error) {
            console.error(error);
            if(error?.code === errorCode.DUPLICATION_KEY_CODE) {
                const keyPatternJson = error.keyPattern;
                const keyName = Object.keys(keyPatternJson)[0].replace('_'," ");
                throw new DuplicationKeyException(keyName)
            }
            throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    async validateUser(payload: JwtPayload): Promise<any> {
        if (payload.consultant_id){
            return this.userModel.findById(payload.id).populate({path:'consultant_id',match: { _id: payload.consultant_id }}).select(['email', 'username', '_id', 'role_name', 'consultant_id']).exec()
        } else {
            return this.userModel.findById(payload.id).select(['email', 'username', '_id', 'role_name', 'consultant_id']);
        }
    }

    async loginUser(email, password, deviceUniqueToken: any, deviceType: any):Promise<any>{
        try {
            const user = await this.userModel.findOne({ email });

            if (user) {
                const isPasswordMatching = await bcrypt.compare(password, user.password);
                if(user && isPasswordMatching){
                    const tokenGenerated = jwtToken.sign({
                        id: user.id,
                        email: user.email,
                        consultant_id:user.consultant_id
                    }, this.configService.get('JWT_SECRET'));
                    const device = await user.logged_devices.find( logged_devices=> ( logged_devices["device_unique_token"] == deviceUniqueToken && logged_devices["device_type"] == deviceType));
                    const updateDeviceIndex =  await user.logged_devices.findIndex( logged_devices=> ( logged_devices["device_unique_token"] == deviceUniqueToken && logged_devices["device_type"] == deviceType));
                    if(device){
                        device.last_loggedin = new Date(Date.now());
                        user.logged_devices[updateDeviceIndex] = device;
                    } else {
                        const newDeviceObj = {
                         device_unique_token : deviceUniqueToken,
                         device_type:deviceType
                        };
                        user.logged_devices.push(newDeviceObj)
                    }

                    user.jwt_token = tokenGenerated;
                    const userUpdate = await this.userModel.updateOne({ _id: user._id }, user);
                    console.log(userUpdate);
                    const tokenObject = {
                        'token': tokenGenerated,
                        'created_at': new Date(),
                        'is_consultant': !!user.consultant_id
                    };
                    return this.Response.returnJSONResponse(successMessages.USER_LOGIN_SUCCESS,HttpStatus.ACCEPTED,tokenObject);
                } else {
                     return   new ForbiddenException('User with this credentials does not exist');
                }
            } else {
                return  new ForbiddenException('User with this credentials does not exist');
            }
        } catch (error) {
            console.error(error);
            if(error?.code === errorCode.DUPLICATION_KEY_CODE) {
                const keyPatternJson = error.keyPattern;
                const keyName = Object.keys(keyPatternJson)[0].replace('_'," ");
                throw new DuplicationKeyException(keyName)
            }
            throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async forgotPassword():Promise<any> {

    }

    async resetPassword():Promise<any> {

    }

    async validateTwoFactorToken():Promise<any>{

    }

    async socialLogin(facebookLoginBody , googleLoginBody) {
        try {
            console.log(facebookLoginBody);
            console.log(googleLoginBody);
            let filter = {

            };
            let updateObject = {

            };
            const existingSocialMediaUser = await this.userModel.findOneAndUpdate(filter,updateObject, {new:true,upsert: true});
            if(existingSocialMediaUser){

            }
              } catch (e) {
            throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    }
