import { IsEmail, IsString, IsNotEmpty, MinLength, MaxLength, IsObject } from 'class-validator';
export class LoginDTO {

    @IsEmail()
    @IsNotEmpty()
    email: string;

    @IsString()
    @IsNotEmpty()
    @MaxLength(25)
    @MinLength(8)
    password: string;

    @IsString()
    @IsNotEmpty()
    deviceType: any;

    @IsString()
    @IsNotEmpty()
    deviceUniqueToken: any;

}
