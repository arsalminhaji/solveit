import { IsEmail, IsString, IsNotEmpty, MinLength, IsNumber, MaxLength } from 'class-validator';
export class RegisterDTO {

  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsString()
  @IsNotEmpty()
  username: string;

  @IsString()
  @IsNotEmpty()
  phone_number:number;


  @IsString()
  @IsNotEmpty()
  @MaxLength(25)
  @MinLength(8)
  password: string;


}

export class facebookLoginDTO {

}

export class googleLoginDTTO{

}
