export interface JwtPayload {
    consultant_id: string;
    id: string;
    email: string;
}
