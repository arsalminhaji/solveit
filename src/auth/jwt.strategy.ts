import { ExtractJwt, Strategy } from 'passport-jwt';

import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { AuthService } from './auth.service';


@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(private readonly authService: AuthService) {
        super({
                jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
                secretOrKey: 'anysecret',
            },

            async (payload: any, next: Function) => await this.validate(payload, next));
    }


    async validate(payload: JwtPayload, done: Function) {
        const user = await this.authService.validateUser(payload);
        console.log("user",user);
        if (!user) {
            return done(new UnauthorizedException(), false);
        }
        done(null, user);
    }
}
