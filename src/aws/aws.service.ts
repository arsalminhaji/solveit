import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { S3 } from 'aws-sdk';
import { v4 as uuid } from 'uuid';
import { type } from 'os';

@Injectable()
export class AwsService {
   s3 = new S3();

  constructor(private readonly configService:ConfigService) {
  }

  //create bucket against envoirnment if dont exisit
  createS3EnvoirnmentBucket(cb) {
      this.s3.listBuckets((err,data) =>{
        if(err){
          console.log("error in bucket creation")
          return cb(err);
        } else {
          const bucketListFilterd = data.Buckets.find(o => o.Name == this.configService.get('AWS_ENVOIRNMENT_BUCKET'));
          if(bucketListFilterd) {
            console.log("Bucket is already created")
          } else {
            const bucketParams = {
              Bucket: this.configService.get('AWS_ENVOIRNMENT_BUCKET'),
              CreateBucketConfiguration: {
                LocationConstraint:this.configService.get('AWS_REGION')
              },
              ACL: "public-read-write"
            };
            this.s3.createBucket(bucketParams, function(err, data) {
              if (err) console.log(err, err.stack); // an error occurred
              else     console.log(data);           // successful response
            });
          }
        }
      });
    }

  getEnvoirnmentBucket():any{
    return this.configService.get('AWS_ENVOIRNMENT_BUCKET')
  }

   uploadFileIntoBucket(dataBuffer,filename,filePath) :Promise<any> {
     return this.s3.upload({
       Bucket: this.getEnvoirnmentBucket(),
       Body: dataBuffer,
       Key: `${filePath}/${uuid()}-${filename}`,
       ACL: 'public-read',
     }).promise().then(sucessUpload => {
       return sucessUpload.Location;
     }).catch((error) => {
      console.log(error);
     });

  }


  async uploadMultipleFiles(files){
    let uploadPush = [];
    for (let i = 0; i < files.length; i++) {
      console.log(files[i]);
      const uploadedFileString = await this.uploadFileIntoBucket(files[i].buffer,files[i].originalname,'problems');
      uploadPush.push(uploadedFileString)
    }
    return uploadPush;
  }

  async deleteFileFromBucket(fileKeywWithPath) :Promise<any> {
    return await this.s3.deleteObject({
      Bucket: this.getEnvoirnmentBucket(),
      Key: fileKeywWithPath
    }).promise();
  }

}
