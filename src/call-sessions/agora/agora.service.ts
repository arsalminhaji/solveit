import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

const {RtcTokenBuilder, RtmTokenBuilder, RtcRole, RtmRole} = require('agora-access-token');


@Injectable()
export class AgoraService {


  constructor(private configService:ConfigService) {
  }
  generateSessionToken(uniqueChannelName){
    const uid = Math.floor(Math.random() * 100);
    const role = RtcRole.PUBLISHER;
    const expirationTimeInSeconds = 86400;
    const channelName = uniqueChannelName ;
    const currentTimestamp = Math.floor(Date.now() / 1000);
    const privilegeExpiredTs = currentTimestamp + expirationTimeInSeconds;
    return {
      serverUserToken : RtcTokenBuilder.buildTokenWithUid(this.configService.get('AGORA_APPID'), this.configService.get('AGORA_CERTIFICATE'), channelName, uid, role, privilegeExpiredTs),
      channelName:channelName,
      serverUserUUID:uid
    }

  }
}
