import { IsDate, IsDateString, IsMongoId, IsNotEmpty, IsString } from 'class-validator';
import { Type } from 'class-transformer';

export class CreateCallSessionsDTO {

    @IsNotEmpty()
    @IsMongoId()
    session_problem_id: string;

    @IsNotEmpty()
    @IsMongoId()
    request_accepted_consultant_id:string;

    @IsNotEmpty()
    @IsDate()
    sessionScheduleRequest

}

export class sessionScheduleRequestDTO {

    @IsNotEmpty()
    @IsMongoId()
    problemId: string;

    @Type(() => Date)
    @IsDate()
    scheduleDateTime:string

}

export class startCallSessionDTO {

    @IsNotEmpty()
    @IsString()
    session_token: string;

    @IsNotEmpty()
    @IsMongoId()
    session_id: string;

    @IsMongoId()
    @IsNotEmpty()
    problem_id:string


}
