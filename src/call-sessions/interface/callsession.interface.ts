import { Document } from 'mongoose'
export interface CallSession extends Document { 
    readonly call_token: string;
    readonly start_time: Date;
    readonly end_time: Date;
    readonly is_expired: boolean;
}