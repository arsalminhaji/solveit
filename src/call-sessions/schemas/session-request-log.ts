import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema  } from 'mongoose';


@Schema()
export class CallSessionRequestLog extends Document {

  @Prop({ type: MongooseSchema.Types.ObjectId,
    ref: 'Problems' ,required:true})
  session_problem_id:string;

  @Prop()
  session_request_status:string;

  @Prop()
  session_request_time:string;


}
export const CallSessionRequestLogSchema = SchemaFactory.createForClass(CallSessionRequestLog);
