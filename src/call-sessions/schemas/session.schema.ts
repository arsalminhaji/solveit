import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';


@Schema()
export class CallSession extends Document {
  @Prop({type:String ,required:true})
  session_token: string;

  @Prop({type:Boolean,default:false,required: true})
  is_session_expired: boolean;

  @Prop({type:Date,required: true})
  session_expiry_date: boolean;

  @Prop({type:String ,required:true})
  session_channel_name:string;

  @Prop({type:Date,default :new Date(Date.now())})
  request_accepted_date :string;

  @Prop({type:Date , required:true})
  start_time:string;

  @Prop({type:Date})
  end_time :string;

  @Prop({type:Date})
  session_scheduled_date:string;

  @Prop({type:Boolean ,default:false})
  is_money_transferred:boolean;

  @Prop({ type: MongooseSchema.Types.ObjectId,
    ref: 'Problems' ,required:true})
  session_problem_id:string;

  @Prop({ type: MongooseSchema.Types.ObjectId,
    ref: 'ConsultantProfile' ,required:true})
  request_accepted_consultant_id:string;

  @Prop({type:[
    raw({
      participant:{ type: MongooseSchema.Types.ObjectId, ref: 'User'},
      agora_token:{ type: raw({
          serverUserToken:{type:String},
          channelName:{type:String},
          serverUserUUID:{type:String}
        })},
    })] ,default:[]})
  session_participants_agora_tokens:Array<Record<string, any>>;

  @Prop({ type: MongooseSchema.Types.ObjectId,
    ref: 'User',required:true})
  owner_id:string;
}

export const CallSessionSchema = SchemaFactory.createForClass(CallSession);
