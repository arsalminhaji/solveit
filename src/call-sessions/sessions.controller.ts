import { Controller, Get, Post, Body, UseGuards, Query, UseInterceptors } from '@nestjs/common';
import { SessionsService } from './sessions.service';
import { CreateCallSessionsDTO, sessionScheduleRequestDTO, startCallSessionDTO } from './dto/create-callsessions.dto';
import { AuthGuard } from '@nestjs/passport';
import { CurrentUser  , CurrentUserConsultantProfile} from '../common/custom-decorators/current.user';
import { listProblemFiltersDTO } from '../problems/dto/problems.filter.dto';
import { ConflictRequest } from '../common/interceptors/conflict.request.interceptor';
import errorMessages from '../common/error.enums';
import { BadRequestInterceptor } from '../common/interceptors/bad.request.interceptor';


@Controller('call-sessions')
export class SessionsController {
  constructor(private readonly sessionsService:SessionsService) {
  }


  @Get('join')
  @UseGuards(AuthGuard('jwt'))
  sessionCallStartOrJoingCall(@CurrentUser() user: any, @Query() sessionsCallStart:startCallSessionDTO){
    return  this.sessionsService.startCallOrJoinCallAndVerifySession(user,sessionsCallStart);
  }


  @Post()
  sessionCallPaused() {
  }

  @Post()
  sessionCallEnd() {
  }

  @Post('request-accepted')
  @UseGuards(AuthGuard('jwt'))
  acceptSessionRequest(@CurrentUser() user: any,@Body() sessionRequestBody:CreateCallSessionsDTO){
    return this.sessionsService.acceptedSessionCreation(user,sessionRequestBody)
  }

  @Post('reschedule')
  @UseGuards(AuthGuard('jwt'))
  rescheduleSession(@CurrentUser() user: any,@Query() sessionId,@Body() sessionRequestBody:CreateCallSessionsDTO){
    return this.sessionsService.acceptedSessionCreation(user,sessionRequestBody,sessionId)
  }


  @Get('')
  @UseGuards(AuthGuard('jwt'))
  GetSessionForUser(@CurrentUser() user: any,@Query() filters:listProblemFiltersDTO ){
    return this.sessionsService.findScheduleCallSessionsByUser(user,filters)
  }

  @Get('consultant')
  @UseGuards(AuthGuard('jwt'))
  GetSessionForConsultant(@CurrentUser() user: any,@Query() filters:listProblemFiltersDTO){
    return this.sessionsService.findScheduleCallSessionByConsultant(user,filters)
  }


  //TODO  add consultant auth guard here or roles guard
  @Post('consultant/request-send')
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(new ConflictRequest(errorMessages.SESSION_ALREADY_EXISIT))
  @UseInterceptors(new BadRequestInterceptor(errorMessages.WRONG_PROBLEM_STATUS))
  SendSessionRequest(@CurrentUser() user: any ,@CurrentUserConsultantProfile() consultant_profile:any ,@Body() sessionScheduleRequest: sessionScheduleRequestDTO) {
      return  this.sessionsService.sendSessionRequest(user,consultant_profile,sessionScheduleRequest)
  }

  @Get('participants')
  sessionParticipants() {
    return 'session'
  }

  @Get('close')
  sessionClosed() {
    return 'session'
  }

}
