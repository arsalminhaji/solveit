import { Module } from '@nestjs/common';
import { SessionsService } from './sessions.service';
import { SessionsController } from './sessions.controller';
import { DatabaseModule } from '../database/database.module';
import {callSessionsProviders} from './sessions.providers'
import { AuthModule } from '../auth/auth.module';
import { ProblemsModule } from '../problems/problems.module';
import { NotificationsModule } from '../notifications/notifications.module';
import { AgoraService } from './agora/agora.service';

@Module({
  imports:[DatabaseModule,AuthModule,ProblemsModule,NotificationsModule],
  controllers:[SessionsController],
  providers: [SessionsService, ...callSessionsProviders, AgoraService],
  exports:[SessionsService, ...callSessionsProviders, ]

})
export class SessionsModule {}
