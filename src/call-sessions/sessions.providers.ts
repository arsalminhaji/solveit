
import { Mongoose } from 'mongoose';
import { CallSessionSchema } from './schemas/session.schema';
import { CallSessionRequestLogSchema } from './schemas/session-request-log';

export const callSessionsProviders = [
  {
    provide: 'CALL_SESSION_MODEL',
    useFactory: (mongoose: Mongoose) => mongoose.model('CallSession', CallSessionSchema),
    inject: ['DATABASE_CONNECTION'],
  },
  {
    provide: 'CALL_SESSION_REQUEST_LOG_MODEL',
    useFactory: (mongoose: Mongoose) => mongoose.model('CallSessionRequestLog', CallSessionRequestLogSchema),
    inject: ['DATABASE_CONNECTION'],
  },
];
