import { Injectable, Inject, HttpStatus, HttpException, BadRequestException } from '@nestjs/common';
import { Model } from 'mongoose';
import { Response } from '../common/response';
import successMessages from '../common/success.message.enums';
import errorMessages from '../common/error.enums';
import { v4 as uuid } from 'uuid';
import {problemStatusEnum,ActionNotificationEnum , SessionRequestEnum} from '../common/common.enum';
import { NotificationsService } from '../notifications/notifications.service';
import { ConflictRequest } from '../common/interceptors/conflict.request.interceptor';
import { startCallSessionDTO } from './dto/create-callsessions.dto';
import { AgoraService } from './agora/agora.service';


@Injectable()
export class SessionsService {

    constructor(@Inject('CALL_SESSION_MODEL') private readonly sessionModel:Model<any>,
                @Inject('PROBLEMS_MODEL') private readonly problemsModel:Model<any>,
                @Inject('CALL_SESSION_REQUEST_LOG_MODEL') private readonly sessionRequestLogModel:Model<any>,
                private readonly notificationsService:NotificationsService,
                private readonly agoraService:AgoraService
    ){
    }

    async acceptedSessionCreation(currentUser:Record<string, any>,createSessionBody:any,sessionId:string =null):Promise<Record<string, any>>{

        try {
            //TODO check if session against that time already schedule or not
            const session = await this.sessionModel.count({$and:[{owner_id:currentUser.id} ,{session_scheduled_date:createSessionBody.scheduleDateTime}]});
            const problemToSolve = await this.problemsModel.findOne({$and:[{_id:createSessionBody.session_problem_id},{owner_id:{$ne:currentUser.id}}]});
            if(sessionId) {
                //TODO add update code only when reschedule
            } else {
                createSessionBody.start_time = null;
                createSessionBody.owner_id = currentUser._id;
                createSessionBody.created_at = new Date(Date.now());
                createSessionBody.session_expiry_date = new Date(Date.now() + 24);
                createSessionBody.session_token = uuid();
                const sessionDoc = new this.sessionModel(createSessionBody);
                const createdSession = await this.sessionModel.create(sessionDoc);
                //TODO  send notification according to settings to problem giver

                return new Response().returnJSONResponse(successMessages.SESSION_ACCEPTED,HttpStatus.OK,createdSession)
            }
        } catch (e) {
            console.error(e);
            throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    async findCallSessionDetailsBySessionId() {

    }

    async findScheduleCallSessionsByUser(currentUser:Record<string, any> , filters:any):Promise<Record<string, any>>{
        try {
            const query = {owner_id:currentUser._id};
            const sort ={ updated_at:1};
            const limit = parseInt(filters.limit, 10) || 10;
            const skip =  (parseInt(filters.page, 10) || 0) * limit;
            const responseFields= {start_time:1,end_time:1 ,session_expiry:1 , session_token :1 ,_id:1};
            const filteredUserProblems = await this.sessionModel.find(query).select(responseFields).lean().sort(sort).skip(skip).limit(limit)
              .populate({path:'session_problem_id',select:{title:1,description:1}})
              .populate({path:'request_accepted_consultant_id',select:{consultant_name:1,consultant_profile_avatar_url:1}}).exec();
            const totalDocCount = await this.sessionModel.countDocuments(query);

            const filterUserProblemResponse ={
                filteredUserSession: filteredUserProblems,
                totalDocCount: totalDocCount,
                page: (parseInt(filters.page, 10) || 0) + 1,
                totalPages:Math.ceil(totalDocCount/limit),
                pageSize:filteredUserProblems.length
            };
            return new Response().returnJSONResponse(successMessages.SESSION_RETRIEVED_SUCCESS,HttpStatus.OK,filterUserProblemResponse)
        } catch (e) {
            console.error(e);
            throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }

    async findScheduleCallSessionByConsultant(currentUser:Record<string, any> , filters:any):Promise<Record<string, any>>{
        try {
            const query = { request_accepted_consultant_id:currentUser.consultant_id};
            const sort ={ updated_at:1 };
            const limit = parseInt(filters.limit, 10) || 10;
            const skip =  (parseInt(filters.page, 10) || 0) * limit;
            const responseFields= { start_time: 1, end_time: 1, status:1,session_expiry:1,session_token :1 ,_id:1};
            const filteredConsultantProblems = await this.sessionModel.find(query).select(responseFields).lean().sort(sort).skip(skip).limit(limit)
              .populate({path:'session_problem_id',select:{title:1,description:1}})
              .populate({path:'owner_id',select:{username:1,user_profile_avatar:1}}).exec();
            const totalDocCount = await this.sessionModel.countDocuments(query);
            const filteredConsultantProblemsResponse ={
                filteredConsultantProblems: filteredConsultantProblems,
                totalDocCount: totalDocCount,
                page: (parseInt(filters.page, 10) || 0) + 1,
                totalPages:Math.ceil(totalDocCount/limit),
                pageSize:filteredConsultantProblems.length
            };
            return new Response().returnJSONResponse(successMessages.SESSION_RETRIEVED_SUCCESS,HttpStatus.OK,filteredConsultantProblemsResponse)

        } catch (e) {
            console.error(e);
            throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async sendSessionRequest(user: any, consultant_profile: any, sessionScheduleRequest: any):Promise<Record<string, any>>{
        try {
            //TODO add time check
            const problem = await this.problemsModel.findOne({$and:[{_id:sessionScheduleRequest.problemId},{owner_id:{$ne:user.id}}]})
              .populate({path:'owner_id', select: {'_id':1, 'email': 1, 'phone_number': 1, 'communication_settings.session_request': 1,logged_devices:1}});
            const session = await this.sessionModel.count({$and:[{owner_id:user.id} ,{session_scheduled_date:sessionScheduleRequest.scheduleDateTime}]});
            if (problem && session == 0) {
                //save session request
                const NotificationData = {
                  SessionRequestTitle:'Session Request',
                  notificationMessage:'Problem Solver'+user.username+ ' has requested a session at '+ sessionScheduleRequest.scheduleDateTime + ' for problem '+ problem.title,
                  notificationIcon:'twitter.svg'
                };
                if(problem.status.includes(problemStatusEnum.APPROVED) || problem.status.includes(problemStatusEnum.SCHEDULED)) {
                    const sessionLogBody= {
                        session_problem_id:problem._id,
                        session_request_status:SessionRequestEnum.PENDING,
                        session_request_time:sessionScheduleRequest.scheduleDateTime
                    };
                    const sessionRequestBuilder  = new this.sessionRequestLogModel(sessionLogBody);
                    const sessionRequest = await this.sessionRequestLogModel.create(sessionRequestBuilder);
                    if(sessionRequest){
                        this.notificationsService.UserCommunicationSettingMapperToSendNotification(problem.owner_id,problem.owner_id.logged_devices,
                          problem.owner_id.email,problem.owner_id.phone_numer,ActionNotificationEnum.SESSION_REQUEST,problem.owner_id.communication_settings.session_request,NotificationData);
                        return  new Response().returnJSONResponse(successMessages.SESSION_REQUEST_SENT,HttpStatus.OK,sessionRequestBuilder)
                    } else {
                        return  new BadRequestException(errorMessages.WRONG_PROBLEM_STATUS)
                    }

                } else {
                    return  new BadRequestException(errorMessages.WRONG_PROBLEM_STATUS)
                }
            } else {
                if( session > 0 ) {
                    return new ConflictRequest(errorMessages.SESSION_ALREADY_EXISIT)
                } else  {
                    return  new Response().returnJSONResponse(successMessages.NO_PROBLEM_EXSIST,HttpStatus.NO_CONTENT,null)
                }
            }
        } catch (e) {
            console.error(e);
            throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async getSessionParticipants() {

    }

    //TODO  verify session is valid ,  participant is genuine and if exisiting then dont generate token.
    async startCallOrJoinCallAndVerifySession(user: any, sessionsCallStart: startCallSessionDTO) {
        try {
            //TODO also add interval time;
            //TODO also add participant check
            const sessionVerification = await this.sessionModel.findOne(
              {$and:[{_id:sessionsCallStart.session_id},
                      {session_token:sessionsCallStart.session_token},
                      {session_problem_id:sessionsCallStart.problem_id}]});
            if(sessionVerification){
                if(sessionVerification['session_participants_agora_tokens'].filter(sessionParticipant => {return sessionParticipant.participant == user.id}).length > 0){
                    const sessionVerificationToken = sessionVerification['session_participants_agora_tokens'].filter(sessionParticipant => {return sessionParticipant.participant == user.id})[0];
                    return new Response().returnJSONResponse(successMessages.SESSION_TOKEN_VERFIED,HttpStatus.OK,sessionVerificationToken)
                } else {
                   const participantToken =  this.agoraService.generateSessionToken(sessionVerification.session_channel_name);
                   const createParticipantToken = {
                       participant:user._id,
                       agora_token:participantToken
                   };
                   sessionVerification['session_participants_agora_tokens'].push(createParticipantToken);
                   const updatedSessionSession = await sessionVerification.save();
                    if(updatedSessionSession) {
                        return new Response().returnJSONResponse(successMessages.SESSION_TOKEN_VERFIED,HttpStatus.OK,createParticipantToken)
                    } else {
                        return  new Response().returnJSONResponse(errorMessages.SAVE_ERROR,HttpStatus.BAD_REQUEST,null)
                    }
                }
            }
        } catch (e) {
            console.error(e);
            throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }



}
