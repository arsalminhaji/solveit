export enum problemStatusEnum  {
  DRAFT="DRAFT",
  CANCELLED = "CANCELLED",
  SCHEDULED ="SCHEDULED",
  RESOLVED = "RESOLVED",
  APPROVED = "APPROVED",
  PENDING = "PENDING"
}

export enum problemChangeEnum  {
  DRAFT="DRAFT",
  CANCELLED = "CANCELLED",
  PENDING = "PENDING"
}
export enum problemApprovalChangeEnum  {
  CANCELLED = "CANCELLED",
  APPROVED = "APPROVED",
}

export enum communicationMediumsEnum {
  SMS="SMS",
  EMAIL="EMAIL",
  PUSH_NOTIFICATIONS="PUSH_NOTIFICATIONS"
}

export enum SessionRequestEnum {
  PENDING="PENDING",
  ACCEPTED="ACCEPTED",
  REJECTED="REJECTED"
}

export enum ActionNotificationEnum {
  SESSION_REQUEST= "session_request",
  SESSION_REQUEST_ACCEPTANCE= "session_request_acceptance",
  PROBLEM_APPROVAL= "problem_approval",
  SESSION_REMINDER = "session_reminder",
  CONSULTANT_PROFILE_APPROVAL="consultant_profile_approval"
}
