enum errorMessages  {
    INTERNAL_SERVER_ERROR = 'Something Went Wrong',
    INCORRECT_PASSWORD = 'Incorrect password',
    LANGUAGE_IS_ALREADY_ADDED = 'Language is already added',
    SESSION_ALREADY_EXISIT="Session already exist against this time",
    WRONG_PROBLEM_STATUS="Problem status is incorrect",
    SAVE_ERROR="Error Saving entity",
    USER_PROFILE_RETRIVED_ERROR="User Profile Retrived"
}
export default errorMessages;
