import { InternalServerErrorException } from '@nestjs/common';

class DuplicationKeyException extends InternalServerErrorException {
  constructor(key_name: string) {
    super(`${key_name}'already exist in the database'`);
  }
}

export default DuplicationKeyException;
