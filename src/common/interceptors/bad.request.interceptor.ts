import {
    BadRequestException,
    CallHandler,
    ExecutionContext,
    Injectable,
    NestInterceptor,
} from '@nestjs/common';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import errorMessages from "../error.enums";

@Injectable()
export class BadRequestInterceptor implements NestInterceptor {
    constructor(private badRequestMessage: string) {
    }

    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        return next.handle()
            .pipe(tap(data => {
                console.log(data);
                if (data.status === 400 && (data.message === errorMessages.LANGUAGE_IS_ALREADY_ADDED || data.message === errorMessages.INCORRECT_PASSWORD || data.message === errorMessages.WRONG_PROBLEM_STATUS)) throw new BadRequestException(this.badRequestMessage);
            }));
    }
}
