import {
  CallHandler, ConflictException,
  ExecutionContext,
  ForbiddenException,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class ConflictRequest implements NestInterceptor {
  constructor(private entityConflictMessage: string) {

  }

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle()
      .pipe(tap(data => {
        console.log(data);
        if (data.status === 409) throw new ConflictException(this.entityConflictMessage);
      }));
  }
}
