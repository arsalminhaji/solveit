import {
  CallHandler,
  ExecutionContext,
  ForbiddenException,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class ForbiddenInterceptor implements NestInterceptor {
  constructor(private userWithThisCredentialsDoesNotExist: string) {

  }

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle()
      .pipe(tap(data => {
        console.log(data);
        if (data.status === 403 && data.message =='User with this credentials does not exist') throw new ForbiddenException(this.userWithThisCredentialsDoesNotExist);
      }));
  }
}
