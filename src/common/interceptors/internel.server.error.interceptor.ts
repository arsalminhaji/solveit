import {
  CallHandler,
  ExecutionContext, ForbiddenException,
  Injectable, InternalServerErrorException,

  NestInterceptor,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class InternelServerErrorInterceptor implements NestInterceptor {
  constructor() {

  }

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle()
      .pipe(tap(data => {
        if (data.status === 500 && data.message =='User with this credentials does not exist') throw new InternalServerErrorException();
      }));
  }
}
