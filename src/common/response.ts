export class Response {


  constructor() {

  }

  public returnJSONResponse(message: string , status: number , data:Record<string, any> | Array<Record<string, any>>):Record<string, any> {
    return { statusCode: status, message: message ,data:data};
  }


}
