enum successMessages {
    USER_SIGNUP_SUCCESS = 'user signup successful',
    USER_LOGIN_SUCCESS = 'user login successful',
    PROBLEM_CREATED_SUCCESS = 'Problem created and updated successful',
    NO_PROBLEM_EXSIST = 'There is no problem exisit given id',
    PROBLEM_RETRIEVED_SUCCESS = 'Problem retrieved successfully',
    SESSION_ACCEPTED = 'Session successfully scheduled',
    CONSULTANT_PROFILE_CREATED = 'Consultant profile created and updated successful',
    SESSION_RETRIEVED_SUCCESS = 'Session retrieved successfully',
    FILE_UPLOAD_SUCCESS = 'Files are uploaded successfully',
    USERNAME_NAME_UPDATED = 'Username updated successfully',
    PHONE_NUMBER_UPDATED = 'Phone number updated successfully',
    PASSWORD_UPDATED = 'Password updated successfully',
    EMAIL_UPDATED = 'Email updated successfully',
    TWO_WAY_AUTHENTICATION = 'Two way authentication updated successfully',
    LANGUAGE_ADDED = 'Language added successfully',
    COUNTRY_UPDATED = 'Country updated successfully',
    FCMTOKEN_UPDATE = 'Fcm token updated',
    SESSION_REQUEST_SENT = 'Session request send',
    SUCCESSFULLY_GET_CURRENT_USER_SETTINGS = 'SUCCESSFULLY_GET_CURRENT_USER_SETTINGS',
    SESSION_REQUEST_UPDATED_SUCCESSFULLY = "Session request updated successfully",
    SESSION_REQUEST_ACCEPTANCE_UPDATED_SUCCESSFULLY = "Session request acceptance updated successfully",
    PROBLEM_APPROVAL_UPDATED_SUCCESSFULLY = "Problem approval updated successfully",
    SESSION_REMAINDER_UPDATED_SUCCESSFULLY = "Session reminder updated successfully",
    CONSULTANT_PROFILE_APPROVAL_UPDATED_SUCCESSFULLY = "Consultant profile approval successfully",
    SESSION_TOKEN_VERFIED = "Session is valid , start the call",
    USER_PROFILE_RETRIVED = "User Profile Retrived successfully",
    ReviewAdded='Review Add',
    ReviewDeleted='Review Deleted',
    ReviewUpdated='ReviewUpdated'

}

export default successMessages;
