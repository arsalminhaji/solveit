import { Test, TestingModule } from '@nestjs/testing';
import { ConsultantProfilesController } from './consultant-profiles.controller';

describe('UserProfiles Controller', () => {
  let controller: ConsultantProfilesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ConsultantProfilesController],
    }).compile();

    controller = module.get<ConsultantProfilesController>(ConsultantProfilesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
