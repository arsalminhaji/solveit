import {Body, Controller, Get, Post, Put, Query, UseGuards} from '@nestjs/common';
import {ConsultantProfilesService} from './consultant-profiles.service';
import {optionalConsultantProfIdDTO} from './dto/optional.consultant-prof.id';
import {addAndUpdateConsultantProfileDTO} from './dto/post.consultant-profile';
import {AuthGuard} from '@nestjs/passport';
import {CurrentUser} from '../common/custom-decorators/current.user';

@Controller('consultant-profiles')
export class ConsultantProfilesController {
    constructor(private readonly consultantProfilesService: ConsultantProfilesService) {
    }

    @Put('')
    @UseGuards(AuthGuard('jwt'))
    addAndUpdateConsultantProfileForReview(@CurrentUser() user: any, @Query() profileStatus, @Body() consultantProfileBody: addAndUpdateConsultantProfileDTO) {
        return this.consultantProfilesService.addAndUpdateConsultantProfile(user, profileStatus, consultantProfileBody)
    }

    @UseGuards(AuthGuard('jwt'))
    @Get('get-profile')
    getConsultantProfile(@CurrentUser() user: any){
        return this.consultantProfilesService.getConsultantProfile(user);
    }

}
