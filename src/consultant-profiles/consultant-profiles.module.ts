import { Module } from '@nestjs/common';
import { ConsultantProfilesController} from './consultant-profiles.controller';
import { ConsultantProfilesService } from './consultant-profiles.service';
import { DatabaseModule } from '../database/database.module';
import { UsersModule } from '../users/users.module';
import {consultantProviders} from './consultant-profiles.providers'
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [DatabaseModule,UsersModule,AuthModule],
  controllers: [ConsultantProfilesController],
  providers: [ConsultantProfilesService,...consultantProviders],
  exports: [ConsultantProfilesService,...consultantProviders],
})
export class ConsultantProfilesModule {}
