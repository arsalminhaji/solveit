import { Mongoose } from 'mongoose';
import {  ConsultantProfilesSchema } from './schemas/consultant-profiles.schema';

export const consultantProviders = [
  {
    provide: 'CONSULTANT_PROFILE_MODEL',
    useFactory: (mongoose: Mongoose) => mongoose.model('ConsultantProfile', ConsultantProfilesSchema),
    inject: ['DATABASE_CONNECTION'],
  },
];
