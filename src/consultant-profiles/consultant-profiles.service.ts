import {HttpException, HttpStatus, Inject, Injectable} from '@nestjs/common';
import {Model, mongo} from 'mongoose';
import {IUser} from '../users/interface/user.interface';
import {Response} from '../common/response';
import successMessages from '../common/success.message.enums';
import errorMessages from '../common/error.enums';
import errorCode from '../common/mongo.error.enum';
import DuplicationKeyException from '../common/exceptions/duplication.key.exception';

@Injectable()
export class ConsultantProfilesService {
    constructor(@Inject('USER_MODEL') private readonly userModel: Model<IUser>
        , @Inject('CONSULTANT_PROFILE_MODEL') private readonly consultantProfModel: Model<any>) {
    }

    async addAndUpdateConsultantProfile(user, profileStatus, addAndUpdateConsultantProfileBody): Promise<Record<string, any>> {
        try {
            let userDetail = await this.userModel.findById(user.id).exec();

            // Add data in consultant profile.
            const query = {_id: userDetail["temp_consultant_id"] ? userDetail["temp_consultant_id"]: new mongo.ObjectID()};
            const options = {upsert: true, new: true, setDefaultsOnInsert: true, fields: {"__v": 0}};
            addAndUpdateConsultantProfileBody["profile_status"] = profileStatus["profile_status"];
            const updateConsultantProfile = await this.consultantProfModel.findByIdAndUpdate(query, addAndUpdateConsultantProfileBody, options).exec();

            // Add consultant id in user model.
            const userIdInsertQuery = {_id: userDetail["_id"]};
            const userFindAndUpdateOptions = {upsert: false, new: true};
            const userFindAndUpdateBody = {temp_consultant_id: updateConsultantProfile["id"]};
            const userUpdatedResponse = await this.userModel.findByIdAndUpdate(userIdInsertQuery, userFindAndUpdateBody, userFindAndUpdateOptions).exec();

            if (userUpdatedResponse && updateConsultantProfile) {
                return new Response().returnJSONResponse(successMessages.CONSULTANT_PROFILE_CREATED, HttpStatus.OK, updateConsultantProfile)
            } else {
                return new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } catch (error) {
            console.error(error);
            if (error?.code === errorCode.DUPLICATION_KEY_CODE) {
                const keyPatternJson = error.keyPattern;
                const keyName = Object.keys(keyPatternJson)[0].replace('_', " ");
                throw new DuplicationKeyException(keyName)
            }
            return new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async getConsultantProfile(user: any){
        let userDetail = await this.userModel.findById(user.id).exec();
        return await this.consultantProfModel.findById(userDetail["temp_consultant_id"]).exec();
    }
}
