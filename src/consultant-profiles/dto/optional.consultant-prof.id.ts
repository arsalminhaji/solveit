import { IsOptional } from 'class-validator';

export class optionalConsultantProfIdDTO {

  @IsOptional()
  consultantProfId: string;

}
