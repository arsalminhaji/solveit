import {IsString, IsNotEmpty, IsOptional} from 'class-validator';

export class addAndUpdateConsultantProfileDTO {
    @IsNotEmpty()
    @IsString()
    @IsOptional()
    about: string;

    @IsNotEmpty()
    @IsOptional()
    education: [{
        degree_name: string
        institution_name: string,
        starting_year: string,
        ending_year: string
    }]

    @IsNotEmpty()
    @IsOptional()
    experience: [{
        organization_name: string
        role: string,
        starting_year: string,
        ending_year: string
    }]

    @IsNotEmpty()
    @IsOptional()
    certification: [{
        certification_name: string
        certification_link: string,
        starting_year: string,
        ending_year: string
    }]

    @IsOptional()
    skill: string[];
}
