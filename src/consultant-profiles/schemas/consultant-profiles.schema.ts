import {Prop, raw, Schema, SchemaFactory} from '@nestjs/mongoose';
import {Document} from 'mongoose';


@Schema()
export class ConsultantProfile extends Document {

    @Prop({type: String})
    about: string;

    @Prop({type: String})
    tem: string;

    @Prop({type: String})
    profile_status: string;

    @Prop(raw({
        degree_name: {type: String},
        institution_name: {type: String},
        starting_year: {type: String},
        ending_year: {type: String}
    }))
    education: Record<string, any>;

    @Prop(raw({
        organization_name: {type: String},
        role: {type: String},
        starting_year: {type: String},
        ending_year: {type: String}
    }))
    experience: Record<string, any>;

    @Prop(raw({
        certification_name: {type: String},
        certification_link: {type: String},
        starting_year: {type: String},
        ending_year: {type: String}
    }))
    certification: Record<string, any>[];

    @Prop({type: Boolean, default: false})
    approval_status: boolean;

    @Prop({type: Array})
    skill: string[];

    
}

export const ConsultantProfilesSchema = SchemaFactory.createForClass(ConsultantProfile);
