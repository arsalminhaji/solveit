import { Controller, Get, UseGuards } from '@nestjs/common';
import { DashboardService } from './dashboard.service';
import { AuthGuard } from '@nestjs/passport';
import { CurrentUser } from '../common/custom-decorators/current.user';

@Controller('dashboard')
export class DashboardController {
  constructor(private readonly dashboardService:DashboardService) {

  }

  @UseGuards(AuthGuard('jwt'))
  @Get('/consultant/tags')
  loadPublicProblemsToSolveAccordingToConsultantTags(@CurrentUser() user: any){
    return this.dashboardService.getPublicProblemsToSolveAccordingToConsultantTags(user)
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('user/view/notags')
  loadPublicProblemsToViewAccordingToNoTags(@CurrentUser() user: any){
    return this.dashboardService.getPublicProblemsToViewAccordingToNoTags(user)
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('user/problemsolver/tags')
  loadPublicProblemSolverProfileWithSkillTagsAndOtherFilter(@CurrentUser() user: any , filter){
    return this.dashboardService.getPublicProblemSolverProfileWithSkillTags(user,filter)
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('search/problem/tags')
  loadPublicProblemToViewOrToSolveByTag(@CurrentUser() user: any , filter){
    return this.dashboardService.getPublicProblemToViewByTags(user,filter)
  }

  // @UseGuards(AuthGuard('jwt'))
  // @Get('search/problem/tags')
  // loadPublicProblemSolverToViewOrToSolveByTag(@CurrentUser() user: any , filter){
  //   return this.dashboardService.(user,filter)
  // }
}
