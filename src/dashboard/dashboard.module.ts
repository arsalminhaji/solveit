import { forwardRef, Module } from '@nestjs/common';
import { DashboardController } from './dashboard.controller';
import { DashboardService } from './dashboard.service';
import { DatabaseModule } from '../database/database.module';
import { AuthModule } from '../auth/auth.module';
import { AwsModule } from '../aws/aws.module';
import { NotificationsModule } from '../notifications/notifications.module';
import { ProblemsModule } from '../problems/problems.module';
import { ConsultantProfilesModule } from '../consultant-profiles/consultant-profiles.module';
import { UsersModule } from '../users/users.module';

@Module({
  imports:[DatabaseModule,AuthModule,AwsModule,ConsultantProfilesModule,ProblemsModule,NotificationsModule,UsersModule],
  controllers: [DashboardController],
  providers: [DashboardService]
})
export class DashboardModule {}
