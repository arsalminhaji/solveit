import { Mongoose } from 'mongoose';
import { ProblemSchema } from '../problems/schemas/problems';
import { UserSchema } from '../users/schemas/user.schema';
import { ConsultantProfilesSchema } from '../consultant-profiles/schemas/consultant-profiles.schema';


export const dashboardProvider = [
  {
    provide: 'PROBLEMS_MODEL',
    useFactory: (mongoose: Mongoose) => mongoose.model('Problems', ProblemSchema),
    inject: ['DATABASE_CONNECTION'],
  },
  {
    provide: 'USER_MODEL',
    useFactory: (mongoose: Mongoose) => mongoose.model('User', UserSchema),
    inject: ['DATABASE_CONNECTION'],
  },
  {
    provide: 'CONSULTANT_PROFILE_MODEL',
    useFactory: (mongoose: Mongoose) => mongoose.model('ConsultantProfile', ConsultantProfilesSchema),
    inject: ['DATABASE_CONNECTION'],
  },
];
