import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Response } from '../common/response';
import successMessages from '../common/success.message.enums';
import errorMessages from '../common/error.enums';
import { Model } from "mongoose";
import { IUser } from '../users/interface/user.interface';

@Injectable()
export class DashboardService {
  constructor(@Inject('PROBLEMS_MODEL') private readonly problemsModel:Model<any>,
              @Inject('USER_MODEL') private readonly userModel: Model<IUser>,
              @Inject('CONSULTANT_PROFILE_MODEL') private readonly consultantProfModel: Model<any>) {
  }
  async getPublicProblemsToSolveAccordingToConsultantTags(currentUser:Record<string, any>){
    try {
      //TODO

    } catch (e) {
      console.error(e);
      throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async getPublicProblemsToViewAccordingToNoTags(currentUser:Record<string, any>){
    try {
      //TODO Add query to get public problems with latest created date  , then add points,  and other filters

    } catch (e) {
      console.error(e);
      throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async getPublicProblemSolverProfileWithSkillTags(currentUser:Record<string, any>,filters:Record<string, any>){
    try {
        //TODO get public problem solver profile using  array  of key words and price limit  or time availiblity
    } catch (e) {
      console.error(e);
      throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


  async getPublicProblemToViewByTags(currentUser:Record<string, any>,filters:Record<string, any>){
    try {
    } catch (e) {
      console.error(e);
      throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }


}
