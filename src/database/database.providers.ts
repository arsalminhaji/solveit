import * as mongoose from 'mongoose';
import {ConfigModule, ConfigService} from '@nestjs/config';

export const databaseProviders = [
    {
        imports: [ConfigModule],
        inject: [ConfigService],
        provide: 'DATABASE_CONNECTION',
        useFactory: async (configService: ConfigService): Promise<typeof mongoose> =>
            await mongoose.connect('mongodb://' + configService.get('DATABASE_HOST') + '/' + configService.get('DATABASE_NAME')),
    },
];
