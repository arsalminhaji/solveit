import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { config } from 'aws-sdk';
import { AwsService } from './aws/aws.service';
import { ServiceAccount } from 'firebase-admin';
import * as admin from 'firebase-admin';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe({
    whitelist: true
  }));
  const configService = await app.get(ConfigService);
  console.log(configService.get('ACCESS_KEY'));
  console.log(configService.get('SECRET_ACCESS'));
  console.log(configService.get('AWS_REGION'));
  await config.update({
    accessKeyId: configService.get('ACCESS_KEY'),
    secretAccessKey: configService.get('SECRET_ACCESS'),
    region: configService.get('AWS_REGION'),
  });
  const  globalConfig = await app.get<ConfigService>(ConfigService);
  const awsSetup = await app.get<AwsService>(AwsService);
  // Set the config options
  const adminConfig: ServiceAccount = {
    "projectId": configService.get<string>('FIREBASE_PROJECT_ID'),
    "privateKey": configService.get<string>('FIREBASE_PRIVATE_KEY')
      .replace(/\\n/g, '\n'),
    "clientEmail": configService.get<string>('FIREBASE_CLIENT_EMAIL'),
  };
  admin.initializeApp({
    credential: admin.credential.cert(adminConfig),
    databaseURL: configService.get<string>('FIREBASE_DB_URL'),
  });
  const cors = {
    origin: '*',
    methods: 'GET, HEAD, PUT, PATCH, POST, DELETE, OPTIONS ,*',
    preflightContinue: false,
    optionsSuccessStatus: 204,
    credentials: true,
    allowedHeaders: [
      'Accept',
      'Content-Type',
      'Authorization',
    ]
  };
  await awsSetup.createS3EnvoirnmentBucket((err)=>{
    console.log(err);
    if(err)  {process.exit(1)}
  });
  await app.enableCors(cors);
  await app.listen(globalConfig.get('SERVER_HOST_PORT'));
}
bootstrap();
