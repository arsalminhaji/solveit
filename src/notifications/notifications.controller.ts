import { Controller, Get, Param, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CurrentUser } from '../common/custom-decorators/current.user';
import { PushNotificationsService } from './push-notifications/push-notifications.service';

@Controller('notifications')
export class NotificationsController {

  constructor(private readonly pushNotificationsService:PushNotificationsService) {
  }

  @Get('')
  @UseGuards(AuthGuard('jwt'))
  getUserNotifications(@CurrentUser() user:any,){
    return this.pushNotificationsService.getUserPushNotifications(user);
  }
}
