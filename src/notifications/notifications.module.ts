import { Module } from '@nestjs/common';
import { NotificationsService } from './notifications.service';
import { MailerModule } from '@nestjs-modules/mailer';
import { PushNotificationsService } from './push-notifications/push-notifications.service';
import { SmsServiceService } from './sms-service/sms-service.service';
import {NotificationsController} from './notifications.controller';
import { notificationProvider } from './notifications.providers';
import { DatabaseModule } from '../database/database.module';

@Module({
  imports:[MailerModule,DatabaseModule],
  providers: [NotificationsService, PushNotificationsService, SmsServiceService,...notificationProvider],
  exports :[NotificationsService,...notificationProvider],
  controllers: [NotificationsController]
})
export class NotificationsModule {}
