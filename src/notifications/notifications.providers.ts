import { Mongoose } from 'mongoose';
import {PushNotificationSchema} from './schema/push-notifications';

export const notificationProvider = [
  {
    provide: 'PUSH_NOTIFICATION',
    useFactory: (mongoose: Mongoose) => mongoose.model('PushNotification', PushNotificationSchema),
    inject: ['DATABASE_CONNECTION'],
  },
];
