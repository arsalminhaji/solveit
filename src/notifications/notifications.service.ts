import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { PushNotificationsService } from './push-notifications/push-notifications.service';
import { SmsServiceService } from './sms-service/sms-service.service';
import { communicationMediumsEnum } from '../common/common.enum';

@Injectable()
export class NotificationsService {
  constructor(private mailerService:MailerService,
              private pushNotificationsService:PushNotificationsService,
              private smsServiceService:SmsServiceService
  ) {}

  UserCommunicationSettingMapperToSendNotification(UserId,Devices,Email,PhoneNumber,ActionNotification,communicationKeyModeArray,Data):void{
    if(communicationKeyModeArray.includes(communicationMediumsEnum.PUSH_NOTIFICATIONS)){
      this.sendPushNotifications(UserId,Devices,ActionNotification,Data);
    }
    if(communicationKeyModeArray.includes(communicationMediumsEnum.EMAIL)){
      this.sendEmail(UserId,Email,ActionNotification,Data);
    }
    if(communicationKeyModeArray.includes(communicationMediumsEnum.SMS,Data)){
      this.sendSMS(UserId,PhoneNumber,ActionNotification,Data);
    }
  }

  sendPushNotifications(userId,devices, ActionNotification, Data: any):void{
    this.pushNotificationsService.sendPushNotification(userId,devices,ActionNotification,Data)
      .then((successMessages)=>{
      console.log(successMessages);
    }).catch((error)=>{
      console.error(error);
    });
  }

  sendEmail(userId,email, communicationKey, Data: any):void{
    const emptyOptions ={
    };
    this.mailerService.sendMail(emptyOptions).then((successMessage)=>{
      console.log(successMessage);
    }).catch((error)=>{
      console.log(error);
    });
  }

  sendSMS(userId,phoneNumber, ActionNotification, Data: any,):void{
    this.smsServiceService.sendSMStoMobile(phoneNumber,ActionNotification)
  }


}
