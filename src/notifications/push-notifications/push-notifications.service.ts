import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import * as admin from 'firebase-admin';
import *  as NotificationBodies from './push_notificaion_content/push_notificaion_bodies'
import { Model } from "mongoose";
import errorMessages from '../../common/error.enums';
import { Response } from '../../common/response';
import successMessages from '../../common/success.message.enums';
var { transform } = require("node-json-transform");


@Injectable()
export class PushNotificationsService {
  constructor(@Inject('PUSH_NOTIFICATION') private readonly pushNotificationModel:Model<any>) {
  }

   async getPushNotificationBodyFromKey(key, data: any) :Promise<Record<any, any>> {
    console.log('key',key);
    const importedJsonKeyTransform = NotificationBodies[key];
     return await transform(data, importedJsonKeyTransform);
   }

  async sendPushNotification(userId,devices,communicationKey,data):Promise<Record<any, any>>{
    const payload = await this.getPushNotificationBodyFromKey(communicationKey,data);
    console.log('payload that will se send',payload);
    const savedPushNotification = await this.savePushNotification(userId,payload);
    const fcmTokenArray = await devices.filter(device => device.device_fcm_token != null).map(device => device.device_fcm_token);
    return  await admin.messaging().sendToDevice(fcmTokenArray,payload)
  }

  savePushNotification(userId,pushNotificationBody) :Promise<Record<any, any>>{
    const pushNotification ={
      owner_id:userId,
      notificationBody:pushNotificationBody
    };
    const createPushNotificationModel = new this.pushNotificationModel(pushNotification);
    return this.pushNotificationModel.create(createPushNotificationModel)
  }

  async getUserPushNotifications(user) :Promise<Record<any, any>>{
    try {
      const query = {owner_id:user._id};
      const sort ={ created_at:1 };
      const pushNotificationArray  =  await this.pushNotificationModel.find(query).sort(sort);
      return new Response().returnJSONResponse(successMessages.PROBLEM_RETRIEVED_SUCCESS,HttpStatus.OK,pushNotificationArray)
    } catch (e) {
      console.error(e);
      throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
