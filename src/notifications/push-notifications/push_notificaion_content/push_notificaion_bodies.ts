const problem_approval = {
  item: {
    data: {
      title :"NotificationTitle",
      body:"notificationMessage",
      click_action: "",
      icon:""

    },
    notification:{
      title :"NotificationTitle",
      body:"notificationMessage",
      click_action: "",
      icon:""
    }
  }
};

const session_request = {
  item: {
    data: {
      title :"SessionRequestTitle",
      body:"notificationMessage",
      click_action: "",
      icon:"notificationIcon"

    },
    notification:{
      title :"SessionRequestTitle",
      body:"notificationMessage",
      click_action: "",
      icon:"notificationIcon"
    }
  }
};

const session_request_acceptance = {
  item: {
    data: {
      title :"problemTitle",
      body:"notificationMessage",
      click_action: "",
      icon:""

    },
    notification:{
      title :"problemTitle",
      body:"notificationMessage",
      click_action: "",
      icon:""
    }
  }
};

const consultant_profile_approval = {
  item: {
    data: {
      title :"ConsultantProfileApprovalTitle",
      body:"notificationMessage",
      click_action: "",
      icon:""

    },
    notification:{
      title :"ConsultantProfileApprovalTitle",
      body:"notificationMessage",
      click_action: "",
      icon:""
    }
  }
};


const session_reminder = {
  item: {
    data: {
      title :"SessionReminder",
      body:"notificationMessage",
      click_action: "",
      icon:""

    },
    notification:{
      title :"SessionReminder",
      body:"notificationMessage",
      click_action: "",
      icon:""
    }
  }
};


export {problem_approval ,session_request ,consultant_profile_approval,session_reminder ,session_request_acceptance};



