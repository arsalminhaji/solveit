import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document,  Schema as MongooseSchema  } from 'mongoose';

@Schema()
export class PushNotification extends Document {

  @Prop({ type: MongooseSchema.Types.ObjectId,
    ref: 'User',default:null})
  owner_id:string;

  @Prop({ type: MongooseSchema.Types.Mixed, required:true})
  notificationBody:string;

  @Prop({type:Date,default : new Date(Date.now())})
  created_at:string;
}

export const PushNotificationSchema = SchemaFactory.createForClass(PushNotification);
