import { IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { problemApprovalChangeEnum, problemChangeEnum } from '../../common/common.enum';

export class optionalIdDTO {

  @IsOptional()
  problemId: string;

}

export class MandatoryIdDTO {

  @IsNotEmpty()
  @IsString()
  problemId: string;

}


export class approvalRequestDTO {

  @IsNotEmpty()
  @IsString()
  problemId: string;

  @IsNotEmpty()
  @IsString()
  adminToken: string;

  @IsNotEmpty()
  @IsString()
  problemToken: string;

  @IsEnum(problemApprovalChangeEnum)
  @IsNotEmpty()
  @IsString()
  problemStatusUpdate

}
