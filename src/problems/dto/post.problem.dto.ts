import { IsString, IsNotEmpty, IsBoolean, IsOptional, IsEnum } from 'class-validator';
import {problemChangeEnum} from '../../common/common.enum';

export class addProblemDTO {

  @IsNotEmpty()
  @IsString()
  title: string;

  @IsString()
  description: string;

  @IsNotEmpty()
  @IsBoolean()
  @IsOptional()
  is_submitted;

  @IsOptional()
  @IsString()
  privacy_status;

  @IsString()
  @IsNotEmpty()
  @IsEnum(problemChangeEnum)
  status;
}
