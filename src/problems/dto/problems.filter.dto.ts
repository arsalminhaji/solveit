import { IsString, IsNotEmpty, IsOptional } from 'class-validator';
export class listProblemFiltersDTO {

  @IsNotEmpty()
  @IsOptional()
  @IsString()
  limit: string;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  page: string;

}
