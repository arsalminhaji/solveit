import {
  Body,
  Controller,
  Get,
  Param,
  Put,
  Query,
  Req,
  UploadedFiles,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { ProblemsService } from './problems.service';
import { addProblemDTO } from './dto/post.problem.dto';
import { MandatoryIdDTO, optionalIdDTO , approvalRequestDTO } from './dto/optional.id.dto';
import { listProblemFiltersDTO } from './dto/problems.filter.dto';
import { AuthGuard } from '@nestjs/passport';
import { CurrentUser } from '../common/custom-decorators/current.user';
import { FilesInterceptor } from '@nestjs/platform-express';
import { BadRequestInterceptor } from '../common/interceptors/bad.request.interceptor';
import errorMessages from '../common/error.enums';


@Controller('problems')
export class ProblemsController {

  constructor(private readonly  problemsService:ProblemsService) {


  }

  @Get('')
  @UseGuards(AuthGuard('jwt'))
  getListOfProblemsAgainstUserId(@CurrentUser() user: any, @Query() filters:listProblemFiltersDTO) {
    return  this.problemsService.getListOfProblemsOfAUser(user,filters);
  }

  @Get('/consultant')
  @UseGuards(AuthGuard('jwt'))
  getListOfProblemsAgainstUserIdAndConsultantId(@CurrentUser() user: any,@Query() filters:listProblemFiltersDTO) {
    return  this.problemsService.getListOfProblemsOfAConsultant(user,filters);
  }

  @Put('')
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(FilesInterceptor('files'))
  addProblem(@CurrentUser() user: any,@Query() id:optionalIdDTO , @Body() problemBody:addProblemDTO,@UploadedFiles() files) {
    const problemId = id.problemId;
    return  this.problemsService.addOrUpdateProblem(user,problemId,problemBody,files)
  }

  @Put('images')
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(FilesInterceptor('files'))
  uploadProblemsFile(@Req() req,@CurrentUser() user: any,@Query() id:MandatoryIdDTO ,@UploadedFiles() files){
    console.log(req.body.files);
    console.log(files);
    if(files.length == 0) {
      files = req.body.files;
    }
    const problemId = id.problemId;
    return  this.problemsService.uploadFileInProblem(user,problemId,files)
  }

  @Get('user/:id')
  @UseGuards(AuthGuard('jwt'))
  getSingleProblem(@CurrentUser() user:any ,@Param() params){
    const problemId  = params.id;
    return this.problemsService.getProblemAgainstProblemId(user,problemId);
  }

  @Get('approval')
  @UseGuards(AuthGuard('jwt'))
  problemApproval(@CurrentUser() user:any ,@Query() problemApprovalQuery:approvalRequestDTO){
    return this.problemsService.problemApprovalProcess(user , problemApprovalQuery)
  }
}
