import { forwardRef, Module } from '@nestjs/common';
import { ProblemsController } from './problems.controller';
import { ProblemsService } from './problems.service';
import {problemsProvider} from './problems.providers';
import { DatabaseModule } from '../database/database.module';
import { AuthModule } from '../auth/auth.module';
import { AwsModule } from '../aws/aws.module';
import { NotificationsModule } from '../notifications/notifications.module';
import { SessionsModule } from '../call-sessions/sessions.module';

@Module({
  imports:[DatabaseModule,AuthModule,AwsModule,NotificationsModule,forwardRef(()=>SessionsModule)],
  controllers: [ProblemsController],
  providers: [ProblemsService,...problemsProvider],
  exports:[ProblemsService,...problemsProvider]
})
export class ProblemsModule {}
