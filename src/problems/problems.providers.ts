import { Mongoose } from 'mongoose';
import { ProblemSchema } from './schemas/problems';

export const problemsProvider = [
  {
    provide: 'PROBLEMS_MODEL',
    useFactory: (mongoose: Mongoose) => mongoose.model('Problems', ProblemSchema),
    inject: ['DATABASE_CONNECTION'],
  },
];
