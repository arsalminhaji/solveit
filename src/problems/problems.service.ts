import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Model, mongo } from 'mongoose';
import { Response } from '../common/response';
import successMessages from '../common/success.message.enums';
import errorMessages from '../common/error.enums';
import { AwsService } from '../aws/aws.service';
import { approvalRequestDTO, MandatoryIdDTO } from './dto/optional.id.dto';
import {problemStatusEnum,ActionNotificationEnum} from '../common/common.enum';
import { NotificationsService } from '../notifications/notifications.service';


@Injectable()
export class ProblemsService {
  constructor(@Inject('PROBLEMS_MODEL') private readonly problemsModel:Model<any>,
              private readonly  awsService:AwsService ,
              @Inject('CALL_SESSION_MODEL') private readonly sessionModel:Model<any>,
              @Inject('CALL_SESSION_REQUEST_LOG_MODEL') private readonly sessionLogModel:Model<any>,

              private readonly notificationsService:NotificationsService) {

  }
  async addOrUpdateProblem(currentUser: Record<string, any>, problemDocId: string, problemBody: Record<string, any>,files = []):Promise<Record<string, any>> {
    try {
      problemBody.owner_id = currentUser._id;
      problemBody.updated_at = new Date(Date.now());
      if (problemBody.is_submitted){
        problemBody.status =problemStatusEnum.PENDING
        //TODO send email to admin user
      }
      if (problemBody.status ==problemStatusEnum.CANCELLED ) {
        problemBody.status = problemStatusEnum.DRAFT
      }
      if(files.length > 0 ){
        problemBody.document_attachments_url  = await this.awsService.uploadMultipleFiles(files);
      }
      const query = { _id : problemDocId ? problemDocId: new mongo.ObjectID() , owner_id:currentUser._id};
      const options = { upsert: true, new: true, setDefaultsOnInsert: true , fields: {title:1,document_attachments_url:1,description:1,_id:1 ,status:1}};
        //TODO  duplicate key error thrown if access by someone else need to think
      const updatedProblemDoc = await this.problemsModel.findOneAndUpdate(query,problemBody,options).exec();
      return new Response().returnJSONResponse(successMessages.PROBLEM_CREATED_SUCCESS,HttpStatus.OK,updatedProblemDoc)
    } catch (e) {
      console.error(e);
      throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

  async getListOfProblemsOfAUser(currentUser:Record<string, any>,filters:Record<string, any>):Promise<Record<string, any>>{
    try {
      const query = {owner_id:currentUser._id};
      const sort ={ updated_at:1 };
      const limit = parseInt(filters.limit, 10) || 10;
      const skip =  (parseInt(filters.page, 10) || 0) * limit;
      const responseFields= { title: 1, description: 1, status:1,views_count:1,privacy_status:1,updated_at:1};
      const filteredUserProblems = await this.problemsModel.find(query).select(responseFields).sort(sort).skip(skip).limit(limit).exec();
      const totalDocCount = await this.problemsModel.countDocuments(query);
      const filterUserProblemResponse ={
        filteredUserProblems: filteredUserProblems,
        totalDocCount: totalDocCount,
        page: (parseInt(filters.page, 10) || 0) + 1,
        totalPages:Math.ceil(totalDocCount/limit),
        pageSize:filteredUserProblems.length
      };
      return new Response().returnJSONResponse(successMessages.PROBLEM_RETRIEVED_SUCCESS,HttpStatus.OK,filterUserProblemResponse)
    } catch (e) {
      console.error(e);
      throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async getListOfProblemsOfAConsultant(currentUser:Record<string, any>,filters:Record<string, any>):Promise<Record<string, any>>{
    try {
      console.log(currentUser);
      const query = { request_accepted_consultant_id:[currentUser.consultant_id]};
      const sort ={ updated_at:1 };
      const limit = parseInt(filters.limit, 10) || 10;
      const skip =  (parseInt(filters.page, 10) || 0) * limit;
      const responseFields= { title: 1, description: 1, updated_at:1};
      const filteredConsultantProblems = await this.problemsModel.find(query).select(responseFields).sort(sort).skip(skip).limit(limit).exec();
      const totalDocCount = await this.problemsModel.countDocuments(query);
      const filteredConsultantProblemsResponse ={
        filteredConsultantProblems: filteredConsultantProblems,
        totalDocCount: totalDocCount,
        page: (parseInt(filters.page, 10) || 0) + 1,
        totalPages:Math.ceil(totalDocCount/limit),
        pageSize:filteredConsultantProblems.length
      };
      return new Response().returnJSONResponse(successMessages.PROBLEM_RETRIEVED_SUCCESS,HttpStatus.OK,filteredConsultantProblemsResponse)

    } catch (e) {
      console.error(e);
      throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  async uploadFileInProblem(currentUser:Record<string, any>,problemDocId: string,files):Promise<Record<string, any>> {
    const uploadedDoc = await this.awsService.uploadMultipleFiles(files);
    return new Response().returnJSONResponse(successMessages.FILE_UPLOAD_SUCCESS,HttpStatus.OK,uploadedDoc)
  }

  async getProblemAgainstProblemId(currentUser: Record<string, any>, problemDocId: MandatoryIdDTO):Promise<Record<string, any>> {
    try {
      const responseFields= { title: 1,document_attachments_url:1, description: 1, updated_at:1 ,status:1};
      const retrievedProblemDoc :any = await  this.problemsModel.findOne({"owner_id":currentUser._id, "_id":problemDocId}).select(responseFields).lean();
      const retrivedSessionFromProblem = await this.sessionModel.find({"owner_id":currentUser._id, "session_problem_id":problemDocId});
      const retrivedSessionRequestLog = await  this.sessionLogModel.find({"session_problem_id":problemDocId});
      if(retrievedProblemDoc) {
        retrievedProblemDoc.session = retrivedSessionFromProblem;
        retrievedProblemDoc.session_request = retrivedSessionRequestLog
        return new Response().returnJSONResponse(successMessages.PROBLEM_RETRIEVED_SUCCESS,HttpStatus.OK,retrievedProblemDoc)
      } else {
        return new Response().returnJSONResponse(successMessages.NO_PROBLEM_EXSIST,HttpStatus.NO_CONTENT,null)
      }
    } catch (e) {
      console.error(e);
      throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

  async problemApprovalProcess(user: any, problemApprovalQuery: approvalRequestDTO):Promise<Record<string, any>>{
    try {
      const query = {_id : problemApprovalQuery.problemId  , problem_token:problemApprovalQuery.problemToken ,status :problemStatusEnum.PENDING };
      const problemBody =  {
        status:problemApprovalQuery.problemStatusUpdate
      };
      const options = { upsert: false, new: true, setDefaultsOnInsert: true , fields: {title:1,document_attachments_url:1,description:1,_id:1 ,status:1}};
      const statusChangedProblem = await this.problemsModel.findOneAndUpdate(query,problemBody,options)
        .populate({path:'owner_id', select: { 'email': 1, 'phone_number': 1, 'communication_settings.problem_approval': 1,logged_devices:1}})
        .exec();
      console.log(statusChangedProblem);
      if(statusChangedProblem) {
        const problemApprovalData = {
          NotificationTitle:'Problem Approval',
          notificationMessage:'Your Problem'+statusChangedProblem.title+' is approved'
        };
        console.log("problem approval data",problemApprovalData);
        this.notificationsService.UserCommunicationSettingMapperToSendNotification(statusChangedProblem.owner_id,statusChangedProblem.owner_id.logged_devices,statusChangedProblem.owner_id.email,
        statusChangedProblem.owner_id.phone_numer,ActionNotificationEnum.PROBLEM_APPROVAL,statusChangedProblem.owner_id.communication_settings.problem_approval,problemApprovalData);
        statusChangedProblem.depopulate('owner_id');
        return new Response().returnJSONResponse(successMessages.PROBLEM_RETRIEVED_SUCCESS,HttpStatus.OK,statusChangedProblem)
      } else {
        return new Response().returnJSONResponse(successMessages.NO_PROBLEM_EXSIST,HttpStatus.NO_CONTENT,null)
      }
    }
    catch (e) {
      console.error(e);
      throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }



}
