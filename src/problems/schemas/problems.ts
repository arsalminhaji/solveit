import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document , Schema as MongooseSchema } from 'mongoose';

@Schema()
export class Problems extends Document {

  @Prop({ type:String, required: true})
  title: string;

  @Prop({ type:String, required: true, lowercase:true })
  description: string;

  @Prop({ type:String , enum :["DRAFT","CANCELLED" ,"SCHEDULED","RESOLVED","APPROVED","PENDING"] ,default :'DRAFT' ,required: true})
  status: string;

  @Prop({type:Boolean,default:false})
  is_submitted:string;

  @Prop({ type: MongooseSchema.Types.ObjectId,
    ref: 'User',default:null})
  owner_id:string;

  @Prop({ type: [MongooseSchema.Types.ObjectId],
    ref: 'User' ,default:[]})
  request_consultants_id:[string];

  @Prop({ type: [MongooseSchema.Types.ObjectId],
    ref: 'User' ,default:[]})
  request_accepted_consultants_id:[string];


  @Prop({type:[String] ,default:[]})
  document_attachments_url:[string];

  @Prop({type:String,required:true ,default : 0 })
  views_count: string;


  @Prop({ type:String , enum :['PRIVATE',"PUBLIC" ] ,default :'PUBLIC' ,required: true})
  privacy_status: string;

  @Prop({type:Date,default : new Date(Date.now())})
  create_at:string;

  @Prop({type:Date,default : new Date(Date.now())})
  updated_at:string
}
export const ProblemSchema = SchemaFactory.createForClass(Problems);
