import {IsString,IsNotEmpty,IsNumber} from 'class-validator';



export class ReviewDto{
    @IsString()
    @IsNotEmpty()
    comments:string;

    @IsString()
    @IsNotEmpty()
    star:string;

}


