import { Controller , Get , Post , Patch, Body, Delete, Param, Put, UseGuards } from '@nestjs/common';
import {Review} from './reviews.model';
import {ReviewsService} from './reviews.service';
import {ReviewDto} from './dto/reviews.dto';
import {AuthGuard} from '@nestjs/passport';

@Controller('reviews')
export class ReviewsController {
    constructor(private readonly reviewService:ReviewsService) {}

    

    @Post('add-reviews')
    @UseGuards(AuthGuard('jwt'))
    createReview(@Body()comment,star:ReviewDto){
        return this.reviewService.createReview(comment,star);
    }
  

    @Delete('delete-reviews/:id')
    @UseGuards(AuthGuard('jwt'))
    deleteReview(@Param('id') id:string){
        console.log(id);
      return this.reviewService.deleteReview(id)
       

    }
    @Put('Update-reviews/:id')
    @UseGuards(AuthGuard('jwt'))
    reviewUpdate(@Param('id') id:string,@Body()reviewDto:ReviewDto){
       return this.reviewService.reviewUpdate(id,reviewDto);

    }
  
    }
