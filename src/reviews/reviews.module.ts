import { Module } from '@nestjs/common';
import { ReviewsController } from './reviews.controller';
import { ReviewsService } from './reviews.service';
import {reviewProviders} from './reviews.providers';
import {DatabaseModule} from '../database/database.module';
import {AuthModule} from '../auth/auth.module';
@Module({
  imports:[DatabaseModule,AuthModule],
  controllers: [ReviewsController],
  providers: [ReviewsService,...reviewProviders],
  exports:[ReviewsService,...reviewProviders]
})
export class ReviewsModule {}
