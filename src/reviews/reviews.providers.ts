import {Mongoose} from 'mongoose';
import { ReviewsSchema} from './schemas/reviews.schema';

export const reviewProviders=[
    {
        provide:'Reviews',
        useFactory:(mongoose:Mongoose)=> mongoose.model('Reviews',ReviewsSchema),
        inject:['DATABASE_CONNECTION'],

    },
];