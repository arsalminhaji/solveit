import { HttpException, HttpStatus, Inject, Injectable ,InternalServerErrorException} from '@nestjs/common';
import {Model} from 'mongoose';
import {Review} from './reviews.model';
import {Response} from '../common/response';
import successMessages from 'src/common/success.message.enums';
import errorCode from '../common/mongo.error.enum';
import errorMessages from '../common/error.enums';
import DuplicationKeyException from '../common/exceptions/duplication.key.exception';
import { ReviewDto } from './dto/reviews.dto';
@Injectable()
export class ReviewsService {
    Response = new Response();
    constructor(@Inject('Reviews')private readonly reviewsModel:Model<any>){}

  
     
async createReview(comment,star) :Promise<Record<string, any>>{
    try{
        const review=await this.reviewsModel.create(comment,star)
        if(review){
            console.log(review)
            return this.Response.returnJSONResponse(successMessages.ReviewAdded,HttpStatus.CREATED,review)
        }
        else{
            return new InternalServerErrorException;

        }

    }catch(error){
        console.error(error);
        if(error?.code === errorCode.DUPLICATION_KEY_CODE){
            const keyPatternJson=error.keyPattern;
            const keyName=Object.keys(keyPatternJson)[0].replace('','');
            throw new DuplicationKeyException(keyName);
        }
        throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR,HttpStatus.INTERNAL_SERVER_ERROR);
    }

        }
       
        async deleteReview(id:string) :Promise<Record<string,any>>{
            try{
                const reviewDelete=await this.reviewsModel.findByIdAndDelete(id)
                if(reviewDelete){
                console.log(reviewDelete)
                return this.Response.returnJSONResponse(successMessages.ReviewDeleted,HttpStatus.OK,reviewDelete)
            }
            else{
                return new InternalServerErrorException;
            }


        }catch(error)
        {
            console.error(error);
            if(error?.code === error.code.DUPLICATION_KEY_CODE)
            {
                const keyPatternJson=error.keyPattern;
                const keyName=Object.keys(keyPatternJson)[0].replace('','');
                throw new DuplicationKeyException(keyName);
            }
            throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR,HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }
    async reviewUpdate(id:string,reviewDto):Promise<Record<string,any>>{
        try{
            const updateReview= await this.reviewsModel.findByIdAndUpdate(id,reviewDto)
        
            if(updateReview){
                console.log(updateReview)
                return this.Response.returnJSONResponse(successMessages.ReviewUpdated,HttpStatus.OK,updateReview)
            
            }
            // else{
            //     return new InternalServerErrorException;
            // }
            
        }
        catch(error)
        {
            console.error(error);
            if(error?.code === error.code.DUPLICATION_KEY_CODE)
            {
                const keyPatternJson=error.keyPattern;
                const keyName=Object.keys(keyPatternJson)[0].replace('','');
                throw new DuplicationKeyException(keyName);
            }
            throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR,HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }
}
