import {Prop, Schema,SchemaFactory} from '@nestjs/mongoose';
import { Document,Mongoose,Schema as MongooseSchema} from 'mongoose';


@Schema()
export class Reviews extends Document{

    @Prop({type:String,required:true})
    comments:string;

    @Prop({type:Number,required:true})
    star:number;

    // @Prop({type:MongooseSchema.Types.ObjectId,
    // ref:'ConsultantProfile',required:true})
    // review_consultant_id:string;

}
export const ReviewsSchema = SchemaFactory.createForClass(Reviews);