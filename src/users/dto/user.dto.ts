import {IsArray, IsBoolean, IsLowercase, IsNotEmpty, IsNumber, IsObject, IsOptional, IsString} from 'class-validator';

export class usernameDto {
    @IsString()
    @IsNotEmpty()
    @IsLowercase()
    username: string;
}

export class phoneNumberDto {
    @IsString()
    @IsNotEmpty()
    phoneNumber: string;
}

export class passwordDto{
    @IsString()
    @IsNotEmpty()
    oldPassword: string;

    @IsString()
    @IsNotEmpty()
    newPassword: string;
}

export class emailDto{
    @IsString()
    @IsNotEmpty()
    email: string;
}

export class twoWayAuthenticationDto{
    @IsBoolean()
    @IsNotEmpty()
    twoWayAuthentication: boolean;
}

export class languageDto{
    @IsString()
    @IsNotEmpty()
    language: string;
}

export class countryDto{
    @IsString()
    @IsNotEmpty()
    country: string;
}

export class sessionRequestDto{
    @IsArray()
    @IsNotEmpty()
    sessionRequest: any;
}

export class consultantProfileApprovalDto{
    @IsArray()
    @IsNotEmpty()
    consultantProfileApproval: any;
}

export class sessionReminderDto{
    @IsArray()
    @IsNotEmpty()
    sessionReminder: any;
}

export class problemApprovalDto{
    @IsArray()
    @IsNotEmpty()
    problemApproval: any;
}

export class sessionRequestAcceptanceDto{
    @IsArray()
    @IsNotEmpty()
    sessionRequestAcceptance: any;
}

export class deviceDTO{
    @IsString()
    @IsNotEmpty()
    deviceType: any;

    @IsString()
    @IsNotEmpty()
    deviceUniqueToken: any;


    @IsString()
    deviceFCMToken: any;

}
