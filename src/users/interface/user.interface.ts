import { Document } from 'mongoose';

export interface IUser extends Document {
  logged_devices: [any];
  consultant_id: any;
  email : string;
  password: string;
  jwt_token: string;
  phone_number: string;
  username: string;
  role_name: string;
}