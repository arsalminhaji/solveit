import { Prop, raw, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';

@Schema()
export class User extends Document {

  @Prop({ type:String, required: true, lowercase:true ,unique:true})
  email: string;

  @Prop({ type:String, required: true, lowercase:true })
  username: string;

  @Prop({ type:String, default:null})
  user_profile_avatar: string;


  @Prop({ type:String,required: true})
  password: string;

  @Prop({type:String, unique:true ,required:true})
  phone_number: string;

  @Prop({type:String , default:'customer'})
  role_name :string;

  @Prop({type:String, default:''})
  jwt_token :string;

  @Prop(raw({
    is_two_factor_auth: {type:Boolean ,default:false},
    languages: {type:[String],default:['en']},
    country:{type:String ,default:'USA'}
  }))
  user_settings:Record<string, any>;


  @Prop(raw({
    session_request:{ type:[String] , enum :["SMS","EMAIL","PUSH_NOTIFICATIONS"] ,default :["PUSH_NOTIFICATIONS"] ,required: true},
    session_request_acceptance:{ type:[String] , enum :["SMS","EMAIL","PUSH_NOTIFICATIONS"] ,default :["PUSH_NOTIFICATIONS","EMAIL"] ,required: true},
    problem_approval:{type:[String] ,enum:["SMS","EMAIL","PUSH_NOTIFICATIONS"], default:["EMAIL","PUSH_NOTIFICATIONS"] ,required:true},
    session_reminder:{type:[String],enum:["SMS","EMAIL","PUSH_NOTIFICATIONS"], default:["SMS","PUSH_NOTIFICATIONS","EMAIL"] ,required:true},
    consultant_profile_approval:{ type:[String],enum:["SMS","EMAIL","PUSH_NOTIFICATIONS"], default:["PUSH_NOTIFICATIONS","EMAIL"],required:true}
  }))
  communication_settings:Record<string, any>;

  @Prop(raw([{
    device_type: { type: String, enum: ["BROWSER", "MOBILE"] },
    last_loggedin:{type:Date , default:Date.now()},
    device_unique_token: { type: String ,required:true},
    device_fcm_token: { type :String ,default :null }
  }]))
  logged_devices:Record<string, any>;

  @Prop({ type: MongooseSchema.Types.ObjectId,
    ref: 'ConsultantProfile',default:null})
  consultant_id:string;

  @Prop({ type: MongooseSchema.Types.ObjectId,
    ref: 'ConsultantProfile',default:null})
  temp_consultant_id:string

}
export const UserSchema = SchemaFactory.createForClass(User);
