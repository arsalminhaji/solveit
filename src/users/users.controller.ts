import {
    Controller,
    Get,
    Body,
    Patch,
    UseGuards, Post, UseInterceptors,
} from '@nestjs/common';
import {UsersService} from './users.service';
import {CurrentUser} from "../common/custom-decorators/current.user";
import {AuthGuard} from "@nestjs/passport";
import {
    consultantProfileApprovalDto,
    countryDto,
    deviceDTO,
    emailDto,
    languageDto,
    passwordDto,
    phoneNumberDto,
    problemApprovalDto,
    sessionReminderDto,
    sessionRequestAcceptanceDto,
    sessionRequestDto,
    twoWayAuthenticationDto,
    usernameDto,
} from './dto/user.dto';
import errorMessages from "../common/error.enums";
import {BadRequestInterceptor} from "../common/interceptors/bad.request.interceptor";
import { InternelServerErrorInterceptor } from '../common/interceptors/internel.server.error.interceptor';

@Controller('users')
export class UsersController {
    constructor(private readonly usersService: UsersService) {
    }

    @Get('settings')
    userSettings() {
        return 'settings'
    }

    @Get('settings/getAllUsers')
    getAllUsers() {
        return this.usersService.getUsers();
    }

    @Patch('settings/username')
    @UseGuards(AuthGuard('jwt'))
    changeUsername(@CurrentUser() user: any,
                   @Body() username: usernameDto) {
        return this.usersService.changeUsername(user, username.username);
    }

    @Patch('settings/phone-number')
    @UseGuards(AuthGuard('jwt'))
    changePhoneNumber(@CurrentUser() user: any,
                      @Body() phoneNumber: phoneNumberDto) {
        return this.usersService.changePhoneNumber(user, phoneNumber.phoneNumber);
    }

    @Patch('settings/password')
    @UseGuards(AuthGuard('jwt'))
    @UseInterceptors(new BadRequestInterceptor(errorMessages.INCORRECT_PASSWORD))
    changePassword(@CurrentUser() user: any,
                   @Body() passwordBody: passwordDto) {
        return this.usersService.changePassword(user, passwordBody.oldPassword, passwordBody.newPassword);
    }

    @Patch('settings/email')
    @UseGuards(AuthGuard('jwt'))
    changeEmail(@CurrentUser() user: any,
                @Body() email: emailDto) {
        return this.usersService.changeEmail(user, email.email);
    }

    @Patch('settings/two-way-authentication')
    @UseGuards(AuthGuard('jwt'))
    changeTwoFactorStatus(@CurrentUser() user: any,
                          @Body() twoWayAuthentication: twoWayAuthenticationDto) {
        return this.usersService.changeTwoWayAuthentication(user, twoWayAuthentication.twoWayAuthentication);
    }

    @Patch('settings/language')
    @UseGuards(AuthGuard('jwt'))
    @UseInterceptors(new BadRequestInterceptor(errorMessages.LANGUAGE_IS_ALREADY_ADDED))
    addLanguage(@CurrentUser() user: any,
                @Body() language: languageDto) {
        return this.usersService.addLanguage(user, language.language);
    }

    @Patch('settings/country')
    @UseGuards(AuthGuard('jwt'))
    changeCountry(@CurrentUser() user: any,
                  @Body() country: countryDto) {
        return this.usersService.changeCountry(user, country.country);
    }

    @Patch('settings/communication-setting/session-request')
    @UseGuards(AuthGuard('jwt'))
    changeSessionRequest(@CurrentUser() user: any,
                         @Body() sessionRequest: sessionRequestDto) {
        return this.usersService.changeSessionRequest(user, sessionRequest.sessionRequest);
    }

    @Patch('settings/communication-setting/session-request-acceptance')
    @UseGuards(AuthGuard('jwt'))
    changeSessionRequestAcceptance(@CurrentUser() user: any,
                                   @Body() sessionRequestAcceptance: sessionRequestAcceptanceDto) {
        return this.usersService.changeSessionRequestAcceptance(user, sessionRequestAcceptance.sessionRequestAcceptance);
    }

    @Patch('settings/communication-setting/problem-approval')
    @UseGuards(AuthGuard('jwt'))
    changeProblemApproval(@CurrentUser() user: any,
                          @Body() problemApproval: problemApprovalDto) {
        return this.usersService.changeProblemApproval(user, problemApproval.problemApproval);
    }

    @Patch('settings/communication-setting/session-reminder')
    @UseGuards(AuthGuard('jwt'))
    changeSessionReminder(@CurrentUser() user: any,
                          @Body() sessionReminder: sessionReminderDto) {
        return this.usersService.changeSessionReminder(user, sessionReminder.sessionReminder);
    }

    @Patch('settings/communication-setting/consultant-profile-approval')
    @UseGuards(AuthGuard('jwt'))
    changeConsultantProfileApproval(@CurrentUser() user: any,
                                    @Body() consultantProfileApproval: consultantProfileApprovalDto) {
        return this.usersService.changeConsultantProfileApproval(user, consultantProfileApproval.consultantProfileApproval);
    }

    @Patch('device/fcm-token')
    @UseInterceptors(new InternelServerErrorInterceptor())
    @UseGuards(AuthGuard('jwt'))
    insertOrUpdateLoggedDevice(@CurrentUser() user: any,
                                    @Body() device: deviceDTO) {
        return this.usersService.insertOrUpdateLoggedDevice(user, device);
    }

    @Get('settings/get-current-user-settings')
    @UseGuards(AuthGuard('jwt'))
    getCurrentUserSetting(@CurrentUser() user: any) {
        return this.usersService.getCurrentUserSetting(user);
    }

    @Get('user-profile')
    @UseGuards(AuthGuard('jwt'))
    getUserProfileShortInfo(@CurrentUser() user: any) {
        return this.usersService.personalDetails(user);
    }



}
