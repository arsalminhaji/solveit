import {
    BadRequestException,
    HttpException,
    HttpStatus,
    Inject,
    Injectable,
    InternalServerErrorException,
} from '@nestjs/common';
import {Model} from "mongoose";
import {IUser} from "./interface/user.interface";
import {Response} from "../common/response";
import successMessages from "../common/success.message.enums";
import errorMessages from "../common/error.enums";
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
    constructor(@Inject('USER_MODEL') private readonly userModel: Model<IUser>) {
    }

    async personalDetails(user:any) {
        try {
            const userProfileDetails = await this.userModel.findById(user.id).select({_id:0,username:1,user_profile_avatar:1}).exec();
            if(userProfileDetails) {
                return new Response().returnJSONResponse(successMessages.USER_PROFILE_RETRIVED, HttpStatus.OK, userProfileDetails)
            } else {
                return new BadRequestException(errorMessages.USER_PROFILE_RETRIVED_ERROR);
            }
        } catch (e) {
            console.log('Error in getting user profile password', e);
            throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    async changePassword(user: any, oldPassword: string, newPassword: string) {
        try {
            let userDetail = await this.userModel.findById(user.id).exec();
            const isPasswordMatch = await bcrypt.compare(oldPassword, userDetail.password);
            if(isPasswordMatch){
                userDetail.password = await bcrypt.hash(newPassword, 10);
                const updatedPassword = await userDetail.save();
                return new Response().returnJSONResponse(successMessages.PASSWORD_UPDATED, HttpStatus.OK, {password: newPassword});
            } else {
                return new BadRequestException(errorMessages.INCORRECT_PASSWORD);
            }
        } catch (e) {
            console.log('Error in updating password', e);
            throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async changeUsername(user: any, username: string) {
        try {
            user.username = username;
            const updatedUser = await user.save();
            return new Response().returnJSONResponse(successMessages.USERNAME_NAME_UPDATED, HttpStatus.OK, {username: updatedUser.username})
        } catch (e) {
            console.log('Error in updating username', e);
            throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async changeEmail(user: any, email: string) {
        try {
            user.email = email;
            const updatedUser = await user.save();
            return new Response().returnJSONResponse(successMessages.EMAIL_UPDATED, HttpStatus.OK, {email: updatedUser.email})
        } catch (e) {
            console.log('Error in updating email', e);
            throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async changeTwoWayAuthentication(user: any, twoWayAuthentication: boolean) {
        try {
            let userDetail = await this.userModel.findById(user.id).exec();
            userDetail["user_settings"]["is_two_factor_auth"] = twoWayAuthentication;
            const updatedUser = await userDetail.save();
            return new Response().returnJSONResponse(successMessages.TWO_WAY_AUTHENTICATION, HttpStatus.OK, {twoWayAuthentication: updatedUser["user_settings"]["is_two_factor_auth"]});
        } catch (e) {
            console.log('Error in updating two way authentication', e);
            throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async changeCountry(user: any, country: string) {
        try {
            let userDetail = await this.userModel.findById(user.id).exec();
            userDetail["user_settings"]["country"] = country;
            const updatedUser = await userDetail.save();
            return new Response().returnJSONResponse(successMessages.COUNTRY_UPDATED, HttpStatus.OK, {country: updatedUser["user_settings"]["country"]});
        } catch (e) {
            console.log('Error in updating country', e);
            throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async addLanguage(user: any, language: string){
        try {
            let userDetail = await this.userModel.findById(user.id).exec();
            const languages = userDetail["user_settings"]["languages"];
            if(!languages.includes(language)){
                languages.push(language);
                const updatedUser = await userDetail.save();
                return new Response().returnJSONResponse(successMessages.LANGUAGE_ADDED, HttpStatus.OK, {languages: updatedUser["user_settings"]["languages"]});
            } else {
                return new BadRequestException(errorMessages.LANGUAGE_IS_ALREADY_ADDED);
            }
        } catch (e) {
            console.log('Error in adding new language', e);
            throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async changeSessionRequest(user: any, sessionRequest: any) {
        try {
            let userDetail = await this.userModel.findById(user.id).exec();
            console.log('here', userDetail["communication_settings"]["session_request"]);
            console.log(sessionRequest);
            userDetail["communication_settings"]["session_request"] = sessionRequest;
            const updatedUser = await userDetail.save();
            return new Response().returnJSONResponse(successMessages.SESSION_REQUEST_UPDATED_SUCCESSFULLY, HttpStatus.OK, {communicationSetting: updatedUser["communication_settings"]["session_request"]});
        } catch (e) {
            console.log('Error in changing session request', e);
            throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async changeSessionRequestAcceptance(user: any, sessionRequestAcceptance: any) {
        try {
            let userDetail = await this.userModel.findById(user.id).exec();
            userDetail["communication_settings"]["session_request_acceptance"] = sessionRequestAcceptance;
            const updatedUser = await userDetail.save();
            return new Response().returnJSONResponse(successMessages.SESSION_REQUEST_ACCEPTANCE_UPDATED_SUCCESSFULLY, HttpStatus.OK, {communicationSetting: updatedUser["communication_settings"]["session_request_acceptance"]});
        } catch (e) {
            console.log('Error in changing session request acceptance', e);
            throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async changeProblemApproval(user: any, problemApproval: any) {
        try {
            let userDetail = await this.userModel.findById(user.id).exec();
            userDetail["communication_settings"]["problem_approval"] = problemApproval;
            const updatedUser = await userDetail.save();
            return new Response().returnJSONResponse(successMessages.PROBLEM_APPROVAL_UPDATED_SUCCESSFULLY, HttpStatus.OK, {communicationSetting: updatedUser["communication_settings"]["problem_approval"]});
        } catch (e) {
            console.log('Error in changing problem approval', e);
            throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async changeSessionReminder(user: any, sessionReminder: any) {
        try {
            let userDetail = await this.userModel.findById(user.id).exec();
            userDetail["communication_settings"]["session_reminder"] = sessionReminder;
            const updatedUser = await userDetail.save();
            return new Response().returnJSONResponse(successMessages.SESSION_REMAINDER_UPDATED_SUCCESSFULLY, HttpStatus.OK, {communicationSetting: updatedUser["communication_settings"]["session_reminder"]});
        } catch (e) {
            console.log('Error in changing session remainder', e);
            throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async changeConsultantProfileApproval(user: any, consultantProfileApproval: any) {
        try {
            let userDetail = await this.userModel.findById(user.id).exec();
            userDetail["communication_settings"]["consultant_profile_approval"] = consultantProfileApproval;
            const updatedUser = await userDetail.save();
            return new Response().returnJSONResponse(successMessages.CONSULTANT_PROFILE_APPROVAL_UPDATED_SUCCESSFULLY, HttpStatus.OK, {communicationSetting: updatedUser["communication_settings"]["consultant_profile_approval"]});
        } catch (e) {
            console.log('Error in changing consultant profile approval', e);
            throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async changePhoneNumber(user: any, phoneNumber: string){
        try {
            let userDetail = await this.userModel.findById(user.id).exec();
            userDetail["phone_number"] = phoneNumber;
            const updatedUser = await userDetail.save();
            return new Response().returnJSONResponse(successMessages.PHONE_NUMBER_UPDATED, HttpStatus.OK, {phoneNumber: updatedUser["phone_number"]});
        } catch (e) {
            console.log('Error in updating phone number', e);
            throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async getUsers() {
        try{
            return await this.userModel.find().exec();
        } catch (e){
            console.log('Error in getting users');
            throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    async insertOrUpdateLoggedDevice(user: any, device: any) {
        try{
            const retrivedUser = await this.userModel.findById(user._id);
            if(retrivedUser){
                const logDevice = await retrivedUser.logged_devices.find( logged_devices=> ( logged_devices["device_unique_token"] == device.deviceUniqueToken && logged_devices["device_type"] == device.deviceType));
                const updateDeviceIndex =  await retrivedUser.logged_devices.findIndex( logged_devices=> ( logged_devices["device_unique_token"] == device.deviceUniqueToken && logged_devices["device_type"] == device.deviceType));
                if(logDevice){
                    logDevice.last_loggedin = new Date(Date.now());
                    logDevice.device_fcm_token = ((device.deviceFCMToken =='null') ? JSON.parse(device.deviceFCMToken) :device.deviceFCMToken);
                    retrivedUser.logged_devices[updateDeviceIndex] = logDevice;
                } else {
                    const newDeviceObj = {
                        device_unique_token : device.deviceUniqueToken,
                        device_type:device.deviceType,
                        device_fcm_token:((device.deviceFCMToken =='null') ? JSON.parse(device.deviceFCMToken) :device.deviceFCMToken)
                    };
                    retrivedUser.logged_devices.push(newDeviceObj)
                }
                const userUpdate = await this.userModel.updateOne({ _id: user._id }, retrivedUser);
                if(userUpdate) {
                    return new Response().returnJSONResponse(successMessages.FCMTOKEN_UPDATE,HttpStatus.OK,null);
                } else {
                    return  new InternalServerErrorException();
                }
            } else {
                return  new InternalServerErrorException();
            }
        } catch (e){
            console.log(e);
            console.log('Error in getting users');
            throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    async getCurrentUserSetting(user: any){
        try {
            let userDetail = await this.userModel.findById(user.id).exec();
            const userSetting = {
                username: userDetail["username"],
                email: userDetail["email"],
                password: userDetail["password"],
                phoneNumber: userDetail["phone_number"],
                userSettings: {
                    twoWayFactor: userDetail["user_settings"]["is_two_factor_auth"],
                    languages: userDetail["user_settings"]["languages"],
                    country: userDetail["user_settings"]["country"]
                },
                communicationSetting: {
                    sessionRequest: userDetail["communication_settings"]["session_request"],
                    sessionRequestAcceptance: userDetail["communication_settings"]["session_request_acceptance"],
                    problemApproval: userDetail["communication_settings"]["problem_approval"],
                    sessionReminder: userDetail["communication_settings"]["session_reminder"],
                    consultantProfileApproval: userDetail["communication_settings"]["consultant_profile_approval"]
                }
            }
            return new Response().returnJSONResponse(successMessages.SUCCESSFULLY_GET_CURRENT_USER_SETTINGS, HttpStatus.OK, userSetting);
        } catch (e) {
            console.log('Error in getting get user setting', e);
            throw new HttpException(errorMessages.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
